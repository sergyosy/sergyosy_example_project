<?php
namespace App\Engines;


class SportCarEngine extends BaseEngine
{
    /**
     * @var string
     */
    protected $type = 'sport';

    /**
     * @var float
     */
    protected $cylinderDiameter = 1.5;

    /**
     * @var int
     */
    protected $cylinderQuantity = 8;

    /**
     * @var float
     */
    protected $volume = 5.0;

    /**
     * @var bool
     */
    protected $turbo = 'true';
}
