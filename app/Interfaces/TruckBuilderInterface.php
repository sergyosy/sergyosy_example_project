<?php
namespace App\Interfaces;

use App\Models\Truck;

/**
 * Interface TruckBuilderInterface
 * @package App\Interfaces
 */
interface TruckBuilderInterface extends SimpleCarBuilderInterface
{
    /**
     * @param bool $isSet
     */
    public function setTrailer(bool $isSet): void;

    /**
     * @param bool $isSet
     */
    public function setBedRoom(bool $isSet): void;
}
