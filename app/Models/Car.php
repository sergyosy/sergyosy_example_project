<?php
namespace App\Models;

use App\Engines\BaseEngine;

/**
 * Class Car
 * @package App\Models
 */
class Car
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var integer
     */
    protected $wheels;

    /**
     * @var BaseEngine
     */
    protected $engine;

    /**
     * @var integer
     */
    protected $seats;

    /**
     * @var bool
     */
    protected $condition;

    /**
     * @var bool
     */
    protected $display;

    //--------- Setters

    public function setType(string $carType)
    {
        $this->type = $carType;
    }

    public function setWheels(int $quantity)
    {
        $this->wheels = $quantity;
    }

    public function setEngine(BaseEngine $engine)
    {
        $this->engine = $engine;
    }

    public function setSeats(int $quantity)
    {
        $this->seats = $quantity;
    }

    public function setCondition(bool $isSet)
    {
        $this->condition = $isSet;
    }

    public function setDisplay(bool $isSet)
    {
        $this->display = $isSet;
    }
}
