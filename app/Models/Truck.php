<?php
namespace App\Models;

/**
 * Class Truck
 * @package App\Models
 */
class Truck extends Car
{
    /**
     * @var bool
     */
    protected $withTrailer;

    /**
     * @var bool
     */
    protected $withBedRoom;

    //--------- Setters

    /**
     * @param bool $isSet
     */
    public function setTrailer(bool $isSet): void
    {
        $this->withTrailer = $isSet;
    }

    /**
     * @param bool $isSet
     */
    public function setBedRoom(bool $isSet): void
    {
        $this->withBedRoom = $isSet;
    }
}
