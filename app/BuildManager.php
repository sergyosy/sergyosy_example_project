<?php
namespace App;

use App\Engines\CarEngine;
use App\Engines\SportCarEngine;
use App\Engines\TruckEngine;
use App\Interfaces\SimpleCarBuilderInterface;
use App\Interfaces\SportCarBuilderInterface;
use App\Interfaces\TruckBuilderInterface;

class BuildManager
{
    public function makeSimpleCar(SimpleCarBuilderInterface $builder)
    {
        $builder->reset();
        $builder->setType('simpleCar');
        $builder->setEngine(new CarEngine());
        $builder->setWheels(4);
        $builder->setSeats(5);
        $builder->setCondition(true);
        $builder->setDisplay(true);

        return $builder->getCar();
    }

    public function makeSportCar(SportCarBuilderInterface $builder)
    {
        $builder->reset();
        $builder->setType('sportCar');
        $builder->setEngine(new SportCarEngine());
        $builder->setWheels(4);
        $builder->setSeats(4);
        $builder->setCondition(false);
        $builder->setDisplay(false);
        $builder->setSpoiler(true);

        return $builder->getCar();
    }

    public function makeTruck(TruckBuilderInterface $builder)
    {
        $builder->reset();
        $builder->setType('truck');
        $builder->setEngine(new TruckEngine());
        $builder->setWheels(10);
        $builder->setSeats(2);
        $builder->setCondition(true);
        $builder->setDisplay(true);
        $builder->setBedRoom(true);
        $builder->setTrailer(true);

        return $builder->getCar();
    }
}
