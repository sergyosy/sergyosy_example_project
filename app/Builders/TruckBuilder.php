<?php
namespace App\Builders;

use App\Engines\BaseEngine;
use App\Interfaces\TruckBuilderInterface;
use App\Models\Truck;

class TruckBuilder implements TruckBuilderInterface
{
    /**
     * @var Truck
     */
    private $truck;

    /**
     * TruckBuilder constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    public function reset(): void
    {
        $this->truck = new Truck();
    }

    /**
     * @param bool $isSet
     */
    public function setCondition(bool $isSet): void
    {
        $this->truck->setCondition($isSet);
    }

    /**
     * @param bool $isSet
     */
    public function setDisplay(bool $isSet): void
    {
        $this->truck->setDisplay($isSet);
    }

    /**
     * @param BaseEngine $engine
     */
    public function setEngine(BaseEngine $engine): void
    {
        $this->truck->setEngine($engine);
    }

    /**
     * @param float $quantity
     */
    public function setSeats(float $quantity): void
    {
        $this->truck->setSeats($quantity);
    }

    /**
     * @param string $carType
     */
    public function setType(string $carType): void
    {
        $this->truck->setType($carType);
    }

    /**
     * @param int $quantity
     */
    public function setWheels(int $quantity): void
    {
        $this->truck->setWheels($quantity);
    }

    /**
     * @param bool $isSet
     */
    public function setTrailer(bool $isSet): void
    {
        $this->truck->setTrailer($isSet);
    }

    /**
     * @param bool $isSet
     */
    public function setBedRoom(bool $isSet): void
    {
        $this->truck->setBedRoom($isSet);
    }

    /**
     * @return Truck
     */
    public function getCar(): Truck
    {
        return $this->truck;
    }
}
