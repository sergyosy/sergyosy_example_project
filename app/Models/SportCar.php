<?php
namespace App\Models;

/**
 * Class SportCar
 * @package App\Models
 */
class SportCar extends Car
{
    /**
     * @var bool
     */
    protected $spoiler;

    //--------- Setters

    /**
     * @param bool $isSet
     */
    public function setSpoiler(bool $isSet): void
    {
        $this->spoiler = $isSet;
    }
}
