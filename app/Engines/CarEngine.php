<?php
namespace App\Engines;


class CarEngine extends BaseEngine
{
    /**
     * @var float
     */
    protected $volume = 1.6;

    /**
     * @var int
     */
    protected $cylinderQuantity = 4;

    /**
     * @var bool
     */
    protected $isHybrid = true;
}
