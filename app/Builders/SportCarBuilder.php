<?php
namespace App\Builders;

use App\Engines\BaseEngine;
use App\Interfaces\SportCarBuilderInterface;
use App\Models\Car;
use App\Models\SportCar;
use App\Models\Truck;

class SportCarBuilder implements SportCarBuilderInterface
{
    /**
     * @var SportCar
     */
    public $sportCar;

    /**
     * CarBuilder constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    public function reset(): void
    {
        $this->sportCar = new SportCar();
    }

    /**
     * @param bool $isSet
     */
    public function setCondition(bool $isSet): void
    {
        $this->sportCar->setCondition($isSet);
    }

    /**
     * @param bool $isSet
     */
    public function setDisplay(bool $isSet): void
    {
        $this->sportCar->setDisplay($isSet);
    }

    /**
     * @param BaseEngine $engine
     */
    public function setEngine(BaseEngine $engine): void
    {
        $this->sportCar->setEngine($engine);
    }

    /**
     * @param float $quantity
     */
    public function setSeats(float $quantity): void
    {
        $this->sportCar->setSeats($quantity);
    }

    /**
     * @param string $carType
     */
    public function setType(string $carType): void
    {
        $this->sportCar->setType($carType);
    }

    /**
     * @param int $quantity
     */
    public function setWheels(int $quantity): void
    {
        $this->sportCar->setWheels($quantity);
    }

    /**
     * @param bool $isSet
     */
    public function setSpoiler(bool $isSet): void
    {
        $this->sportCar->setSpoiler($isSet);
    }

    /**
     * @return SportCar
     */
    public function getCar(): SportCar
    {
        return $this->sportCar;
    }
}
