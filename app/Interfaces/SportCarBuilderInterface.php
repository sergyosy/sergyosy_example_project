<?php
namespace App\Interfaces;

use App\Models\SportCar;

/**
 * Interface SportCarBuilderInterface
 * @package App\Interfaces
 */
interface SportCarBuilderInterface extends SimpleCarBuilderInterface
{
    /**
     * @param bool $isSet
     */
    public function setSpoiler(bool $isSet): void;
}
