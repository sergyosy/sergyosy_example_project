<?php
namespace App\Builders;

use App\Engines\BaseEngine;
use App\Interfaces\SimpleCarBuilderInterface;
use App\Models\Car;

/**
 * Class SimpleCarBuilderInterface
 * @package App\Builders
 */
class SimpleCarBuilder implements SimpleCarBuilderInterface
{
    /**
     * @var Car
     */
    public $car;

    /**
     * SimpleCarBuilderInterface constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    public function reset(): void
    {
        $this->car = new Car();
    }

    /**
     * @param bool $isSet
     */
    public function setCondition(bool $isSet): void
    {
        $this->car->setCondition($isSet);
    }

    /**
     * @param bool $isSet
     */
    public function setDisplay(bool $isSet): void
    {
        $this->car->setDisplay($isSet);
    }

    /**
     * @param BaseEngine $engine
     */
    public function setEngine(BaseEngine $engine): void
    {
        $this->car->setEngine($engine);
    }

    /**
     * @param float $quantity
     */
    public function setSeats(float $quantity): void
    {
        $this->car->setSeats($quantity);
    }

    /**
     * @param string $carType
     */
    public function setType(string $carType): void
    {
        $this->car->setType($carType);
    }

    /**
     * @param int $quantity
     */
    public function setWheels(int $quantity): void
    {
        $this->car->setWheels($quantity);
    }

    /**
     * @return Car
     */
    public function getCar(): Car
    {
        return $this->car;
    }
}
