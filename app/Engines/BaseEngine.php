<?php
namespace App\Engines;


class BaseEngine
{
    /**
     * @var string
     */
    protected $type = 'base';

    /**
     * @var float
     */
    protected $cylinderDiameter = 1;

    /**
     * @var integer
     */
    protected $cylinderQuantity = 3;

    /**
     * @var float
     */
    protected $volume = 0.89;

    /**
     * @var string
     */
    protected $fuel = 'petrol';
}
