<?php
namespace App\Http\Controllers;

use App\Builders\SimpleCarBuilder;
use App\Builders\SportCarBuilder;
use App\Builders\TruckBuilder;
use App\BuildManager;

/**
 * Class MainController
 * @package App\Http\Controllers
 */
class MainController
{
    public function makeCar()
    {
        $builder = new BuildManager();

        $result = $builder->makeSimpleCar(new SimpleCarBuilder());

        dd($result);
    }

    public function makeSportCar()
    {
        $builder = new BuildManager();

        $result = $builder->makeSportCar(new SportCarBuilder());

        dd($result);
    }

    public function makeTruck()
    {
        $builder = new BuildManager();

        $result = $builder->makeTruck(new TruckBuilder());

        dd($result);
    }
}
