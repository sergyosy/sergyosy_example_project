<?php
namespace App\Interfaces;

use App\Engines\BaseEngine;
use App\Models\Car;
use App\Models\SportCar;
use App\Models\Truck;

/**
 * Interface SimpleCarBuilderInterface
 * @package App\Interfaces
 */
interface SimpleCarBuilderInterface
{
    public function reset(): void;

    /**
     * @param string $carType
     */
    public function setType(string $carType): void;

    /**
     * @param int $quantity
     */
    public function setWheels(int $quantity): void;

    /**
     * @param BaseEngine $engine
     */
    public function setEngine(BaseEngine $engine): void;

    /**
     * @param float $quantity
     */
    public function setSeats(float $quantity): void;

    /**
     * @param bool $isSet
     */
    public function setCondition(bool $isSet): void;

    /**
     * @param bool $isSet
     */
    public function setDisplay(bool $isSet): void;

    /**
     * @return Car|SportCar|Truck
     */
    public function getCar();

}
